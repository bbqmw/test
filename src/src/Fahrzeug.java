public class Fahrzeug {

    // Attribute
    public String name;
    private String farbe;
    protected String model;
    public int ps;
    public int baujahr;
    public boolean kraftstoff;

    // Konstruktor

    public Fahrzeug() {}
    public Fahrzeug(String name) {
        this.name = name;
    }
    public Fahrzeug(String name, String farbe) {
        this.name = name;
        this.farbe = farbe;
    }
    public Fahrzeug(String name, String farbe, String model, int ps, int baujahr, boolean kraftstoff) {
        this.name = name;
        this.farbe = farbe;
        this.model = model;
        this.ps = ps;
        this.baujahr = baujahr;
        this.kraftstoff = kraftstoff;
    }

    // Methoden

    public String infoAusgeben() {
        String output = "Name: " + this.name + "\n";
        output += "Farbe: " + this.getFarbe() + "\n";
        output += "Model: " + this.model + "\n";
        output += "PS: " + this.ps + "\n";
        output += "Baujahr: " + this.baujahr + "\n";
        if (this.kraftstoff) {
            output += "Kraftstoff: Elektro";
        } else {
            output += "Kraftstoff: Abgase";
        }
        return output;
    }

    public void fliegen() {}
    public void fahren() {}
    public void bremsen() {}
    public void abbiegen() {}

    public String getFarbe() {
        return this.farbe;
    }

}
